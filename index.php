<?php

  include 'config/conn.php';

?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>APOR REPORT</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="assets/font/css/all.css">

    <meta name="theme-color" content="#563d7c">

  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">APOR Report</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <!-- <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> -->
          </li>
        </ul>
        <div class="form-inline mt-2 mt-md-0">
          <?php if($_SESSION["logged_in"] == 1){ ?>
            <button class="btn btn-outline-danger my-2 my-sm-0" type="button" onclick="log_out()">Logout</button>
          <?php } ?>
        </div>
      </div>
    </nav>

    <?php if($_SESSION["logged_in"] == 0){ ?>
      <main id="main-login" role="main" class="pt-5">
        <div class="col-md-8 offset-md-2 mt-5">
          <form class="form-signin col-md-4 offset-md-4" id="loginForm" method="POST" action="" autocomplete="false">
            <div class="text-center mb-4">
              <h1 class="h3 mb-3">SK FEDERATION - BACOLOD <br> APOR REPORT</h1>
            </div>

            <div class="form-label-group">
              <input type="text" id="inputUsername" class="form-control" name="username" placeholder="Username" required="" autofocus="">
              <label for="inputEmail">Username</label>
            </div>

            <div class="form-label-group">
              <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required="">
              <label for="inputPassword">Password</label>
            </div>

            <button class="btn btn-lg btn-primary btn-block btn-login" type="submit">Sign in</button>
            <p class="mt-5 mb-3 text-muted text-center">© 2020</p>
          </form>
        </div>
      </main>
    <?php }else{ ?>
      <main id="main-add" role="main" class="pt-5">
        <div class="container mt-5">
          <div class="col-md-6 offset-md-3">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">Date: </span>
              </div>
              <input type="date" class="form-control" id="get_date">
              <div class="input-group-append">
                <button type="button" class="btn btn-primary" onclick="filter_by_date()"><i class="fa fa-search"></i> Filter</button>
              </div>
            </div>
          </div>
          <div class="row">
            <h3 class="col-md-6">APOR List</h3>
            <div class="col-md-6">
              <button type="button" class="btn btn-danger float-right ml-1" onclick="delete_data()"><i class="fa fa-trash"></i> Delete</button>
              <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addMD"><i class="fa fa-plus"></i> Add New</button>
            </div>
          </div>
          <table id="tbl_apor_list" class="table table-striped">
            <thead class="bg-dark text-white">
              <tr>
                <th scope="col" width="5px"><input type="checkbox" id="checkAll" onclick="checkAll()"></th>
                <th scope="col" width="5px">#</th>
                <th scope="col">Name</th>
                <th scope="col">Place of Origin</th>
                <th scope="col" width="150px">Date of Arrival</th>
                <th scope="col" width="10px">Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

          <hr>
        </div> <!-- /container -->

        <!-- Add Modal -->
        <div class="modal fade" id="addMD" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus"></i> Add New</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="addAPORForm" method="POST" action="" autocomplete="false">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="aName" name="aName" required="">
                  </div>
                  <div class="form-group">
                    <label for="placeOfOrigin">Place of Origin</label>
                    <input type="text" class="form-control" id="placeOfOrigin" name="placeOfOrigin" required="">
                  </div>
                  <div class="form-group">
                    <label for="dateOfArrival">Date of Arrival</label>
                    <input type="date" class="form-control" id="dateOfArrival" name="dateOfArrival" required="">
                  </div>
                  <hr>
                  <div class="float-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!-- Add Modal End -->

        <!-- Edit Modal -->
        <div class="modal fade" id="editMD" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-id-card"></i> APOR Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form id="updateAPORForm" method="POST" action="">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="hidden" class="form-control" id="edit_aID" name="aID" required="">
                    <input type="text" class="form-control" id="edit_aName" name="aName" required="">
                  </div>
                  <div class="form-group">
                    <label for="placeOfOrigin">Place of Origin</label>
                    <input type="text" class="form-control" id="edit_placeOfOrigin" name="placeOfOrigin" required="">
                  </div>
                  <div class="form-group">
                    <label for="dateOfArrival">Date of Arrival</label>
                    <input type="date" class="form-control" id="edit_dateOfArrival" name="dateOfArrival" required="">
                  </div>
                  <hr>
                  <div class="float-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Edit Modal End -->

        

      <footer class="container">
        <p>© SK Federation - Bacolod, 2020</p>
      </footer>

      </main>
    <?php } ?>

    <script type="text/javascript" src="assets/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/dataTables.bootstrap4.min.js"></script> 
    <script type="text/javascript" src="assets/js/buttons.bootstrap4.min.js"></script> 
    <script type="text/javascript" src="assets/js/buttons.print.min.js"></script> 
    <script type="text/javascript" src="assets/js/dataTables.buttons.min.js"></script> 
    <script type="text/javascript" src="assets/font/js/all.js"></script>
    <script type="text/javascript">
      $(document).ready( function(){
        getAporList("");
      });

      function getAporList(arrivalDate){
        $("#tbl_apor_list").DataTable().destroy();
        $("#tbl_apor_list").DataTable({
          "processing": true,
          "ajax": {
              "url": "ajax/apor_list_data.php",
              "type": "POST",
              "data": {arrivalDate: arrivalDate}
          },
          "columns": [
              { 
                "mRender": function(data, type, row){
                  return "<input type='checkbox' name='aporList' value='"+row.aID+"'>";
                } 
              },
              { "data": "count" },
              { "data": "aName" },
              { "data": "placeOfOrigin" },
              { "data": "arrivalDate" },
              { 
                "mRender": function(data, type, row){
                  return "<button type = 'button' class='btn btn-sm btn-dark' onclick='edit_data("+row.aID+")'>Details</button>";
                } 
              }
          ],
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'print',
              title: 'APOR REPORT (<?php echo date("Y-m-d")?>)',
              exportOptions: {
                  columns: [1,2,3,4]
              }
            }
          ]
        });
      }

      function filter_by_date(){
        var date = $("#get_date").val();
        getAporList(date);
      }

      function checkAll(){
        var x = $("#checkAll").is(":checked");
        if(x){
          $("input[name=aporList]").prop("checked",true);
        }else{
          $("input[name=aporList]").prop("checked",false);
        }
      }

      function delete_data(){
        var aID = [];
        $("input[name=aporList]:checked").each( function(){
          aID.push($(this).val());
        });
         var x = confirm("Are you sure to delete selected data?");
         var url = "ajax/delete_apor_data.php";

         if(x && aID != ""){
            $.ajax({
              type: "POST",
              data: {aID: aID},
              url: url,
              success: function(data){
                  if(data == 1){
                      alert("Success! Selected data was deleted.");
                      getAporList();
                  }else{
                      alert("Error"+data);
                  }
              }
            });
         }
      }

      function edit_data(aID){
        $("#editMD").modal();
        fetch_data(aID);
      }

      function fetch_data(id){
        var url = "ajax/get_apor_data.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {id: id},
          success: function(data){
            var row = JSON.parse(data);
            $("#edit_aID").val(row.aID);
            $("#edit_aName").val(row.aName);
            $("#edit_placeOfOrigin").val(row.placeOfOrigin);
            $("#edit_dateOfArrival").val(row.arrivalDate);
          }
        });
      }

      $("#addAPORForm").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "ajax/add_apor_data.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success! APOR data was added.");
              getAporList();
              $("input").val("");
              $("#addMD").modal("hide");
            }else{
              alert("Error:"+data);
            }
          }
        });
      });

      $("#updateAPORForm").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "ajax/update_apor_data.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success! APOR data was updated.");
              getAporList();
              $("input").val("");
              $("#editMD").modal("hide");
            }else{
              alert("Error:"+data);
            }
          }
        });
      });

      $("#loginForm").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "ajax/auth.php";
        $(".btn-login").prop("disabled", true);
        $(".btn-login").html("Logging in...");
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              setTimeout( function(){
                window.location.reload();
              },2000);
            }else if(data == 0){
              alert("Username or Password doesn't match.");
              $(".btn-login").prop("disabled", false);
              $(".btn-login").html("Sign in");
            }else{
              alert("Error:"+data);
              $(".btn-login").prop("disabled", false);
              $(".btn-login").html("Sign in");
            }
          }
      });
    });

    function log_out(){
      var x = confirm("Are you sure to end your session?");
      if(x){
        window.location="ajax/logout.php";
      }
    }

    </script>

  </body>
</html>